### GitLab Issue Bash Prize Winner Calculator

Here we publish how the prize winners for GitLab issue bashes are chosen.

Our aim is to make the prize winning process as fair as possible for all members involved in the event

All suggestions for how to make the prize winning process as fair as possible are welcomed. Please raise an issue.
- https://gitlab.com/gitlab-org/issue-bash/prize-winner-calculator/issues/new

We expect the calculation to evolve for a while